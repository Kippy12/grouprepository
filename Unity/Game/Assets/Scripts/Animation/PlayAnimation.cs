using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimation : MonoBehaviour
{
    [SerializeField] private Animator AnimObject = null;

    [SerializeField] private string AnimName = "";


    private void PlayAnim()
    {
        AnimObject.Play(AnimName, 0, 0.0f);
    }

    
}
