﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    [SerializeField] private string mouseXInputName;
    [SerializeField] private string mouseYInputName;
    [SerializeField] private float mouseSensitivity;
    [SerializeField] private Transform playerBody;

    private float xAxisRotation;


    private void Awake()
    {
        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        xAxisRotation = 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Handle camera rotation

        float mouseX = Input.GetAxis(mouseXInputName) * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis(mouseYInputName) * mouseSensitivity * Time.deltaTime;

        xAxisRotation += mouseY;

        if (xAxisRotation > 90.0f)
        {
            xAxisRotation = 90.0f;
            mouseY = 0.0f;
        }
        else if (xAxisRotation < -90.0f)
        {
            xAxisRotation = -90.0f;
            mouseY = 0.0f;
        }

        transform.Rotate(Vector3.left * mouseY);
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
