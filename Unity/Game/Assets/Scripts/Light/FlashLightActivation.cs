using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightActivation : MonoBehaviour
{
    public bool FlashOn = true;

    public GameObject Flashlight;
    public GameObject Battery;
    public GameObject FlashlightUI;
    public GameObject BatteryUI;

    public bool failSafe = false;
    public bool isOutOfPower = false;
    public int BatteryLife = 5;

    void Start()
    {
        BatteryUI.SetActive(true);
    }

    void Update()
    {
        if (Input.GetButtonDown("FKey"))
        {
            if (FlashOn == false && failSafe == false && isOutOfPower == false)
            {
                if (BatteryLife <= 0)
                {
                    Debug.Log("OUT OF POWER");
                    StartCoroutine(BatteryPower());
                    BatteryUI.SetActive(false);
                }
                else
                {
                    failSafe = true;
                    Flashlight.SetActive(true);
                    FlashlightUI.SetActive(true);
                    FlashOn = true;
                    Debug.Log("ON");
                    StartCoroutine(FailSafe());
                    BatteryLife = BatteryLife - 1;
                    BatteryUI.SetActive(true);

                    StartCoroutine(TimeLimit());
                }
            }

            else if (FlashOn == true && failSafe == false)
            {
                LightOff();
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Battery")
        {
            BatteryLife = BatteryLife + 5;
            if (BatteryLife > 5)
            {
                BatteryLife = 5;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Battery")
        {
            BatteryLife = BatteryLife + 3;
            if (BatteryLife > 5)
            {
                BatteryLife = 5;
            }
        }
    }

    public void LightOff()
    {
        failSafe = true;
        Flashlight.SetActive(false);
        FlashlightUI.SetActive(false);
        FlashOn = false;
        Debug.Log("OFF");
        StartCoroutine(FailSafe());
    }

    public IEnumerator FailSafe()
    {
        yield return new WaitForSeconds(0.25f);
        failSafe = false;
    }

    public IEnumerator BatteryPower()
    {
        for (int i = 0; i < BatteryLife; i++)
        {
            yield return new WaitForSeconds(0);

            LightOff();
            if (BatteryLife <= 0)
            {
                Debug.Log("OUT OF POWER");
                StartCoroutine(BatteryPower());
                BatteryUI.SetActive(false);
            }
        }
    }

    public IEnumerator TimeLimit()
    {
        yield return new WaitForSeconds(10);
        LightOff();
    }
}
