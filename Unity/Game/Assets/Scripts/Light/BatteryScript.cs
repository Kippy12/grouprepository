using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryScript : MonoBehaviour
{
    //public FlashLightActivation flact;
    //public GameObject BatteryUI;
    //bool hasBatteries;
    public int InstallTime;

    // Update is called once per frame
    void Update()
    {
        //if (hasBatteries == true)
        //{
        //    BatteryUI.SetActive(true);
        //}
        //else if (hasBatteries == false)
        //{
        //    BatteryUI.SetActive(false);
        //}
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            StartCoroutine(Used());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(Used());
        }
    }

    public IEnumerator Used()
    {
        yield return new WaitForSeconds(InstallTime);
        Destroy(gameObject);
    }
}
