using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pauses : MonoBehaviour
{
    public string mainMenuScene;
    public GameObject Pause;
    public bool Paused = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Paused)
            {
                Paused = false;
                Pause.SetActive(false);
                Time.timeScale = 1f;
                Cursor.visible = true;
            }
            else
            {
                Paused = true;
                Pause.SetActive(true);
                Time.timeScale = 0f;
            }
        }
    }
    void Resume()
    {
        Paused = false;
        Pause.SetActive(false);
        Time.timeScale = 1f;
    }

    void MainMenu(string SceneID)
    {
        SceneManager.LoadScene(SceneID);
    }
}
