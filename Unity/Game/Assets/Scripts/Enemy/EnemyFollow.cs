using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyFollow : MonoBehaviour
{

    public Transform playerPos;
    public float speed = 4f;
    Rigidbody rig;
    public int staggerTime;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float distance = Vector3.Distance(playerPos.position, transform.position);

        if (distance < 20)
        {
            //Enemy Moves towards player
            Vector3 pos = Vector3.MoveTowards(transform.position, playerPos.position, speed * Time.fixedDeltaTime);
            rig.MovePosition(pos);
            transform.LookAt(playerPos);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Light")
        {
            speed = 1f;
            StartCoroutine(FailSafe());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Light")
        {
            speed = 1f;
            StartCoroutine(FailSafe());
        }
    }


    void OnCollisionEnter2(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    void OnTriggerEnter2(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("MainMenu");
        }
    }



    IEnumerator FailSafe()
    {
        yield return new WaitForSeconds(staggerTime);
        speed = 4f;
    }

}
