using KeySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace KeySystem
{
    public class KeyItemController : MonoBehaviour
    {
        [SerializeField] private bool redDoor = false;
        [SerializeField] private bool redKey = false;

        [SerializeField] private KeyInventory _KeyInventory = null;

        private KeyDoorController doorObject;
        public TextMeshProUGUI KeyNotif;
        public GameObject KeyN;

        private void Start()
        {
            if (redDoor)
            {
                doorObject = GetComponent<KeyDoorController>();
            }

        }

        public void ObjectInteraction()
        {
            if(redDoor)
            {
                doorObject.PlayAnimation(); 
            }

            else if (redKey)
            {
                _KeyInventory.hasRedKey = true;
                KeyNotif.enabled = true;
                KeyN.SetActive(true);
                StartCoroutine(Notification());
                
                gameObject.SetActive(false);
                
            }
        }

        IEnumerator Notification()
        {
            KeyNotif.canvasRenderer.SetAlpha(0.0f);

            FadeIn();
            yield return new WaitForSeconds(3.5f);

            FadeOut();
            yield return new WaitForSeconds(2.5f);

            KeyN.SetActive(false);
        }

        void FadeIn()
        {
            KeyNotif.CrossFadeAlpha(1.0f, 1.5f, false);
            FadeOut();
        }

        void FadeOut()
        {
            KeyNotif.CrossFadeAlpha(0.0f, 2.5f, true);
            KeyN.SetActive(false);
        }
    }
}